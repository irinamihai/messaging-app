import peewee
from peewee import *
from msg_logging import *

import MySQLdb
import os
import time

print os.environ['MYSQL_USER']
print os.environ['MYSQL_PASSWORD']
print os.environ['MSG_API_DB_HOST']

db = MySQLDatabase('message',
                   host=os.environ['MSG_API_DB_HOST'],
                   port=3306,
                   user=os.environ['MYSQL_USER'],
                   passwd=os.environ['MYSQL_PASSWORD'])

class Base(peewee.Model):
    class Meta:
        database = db
        
class User(Base):
    username = CharField(unique=True)
    
class Message(Base):
    text = TextField()
    user_id = ForeignKeyField(User, backref='messages')

def init_tables():
    set_loglevel(LOG_INFO)
    set_logfile("server_log.txt")

    log(LOG_INFO, "Create needed tables")

    # When running with K8S, it takes a while for the MySQL pod to start
    # and configure. We're giving it at most 10 minutes because we're
    # generous today.
    created_user = False
    created_message = False
    max_retries = 60
    retries = 0

    # Initializing database with the required tables.
    while True:
        try:
            log(LOG_INFO, "Create table users")
            User.create_table()
            created_user = True
        except peewee.OperationalError as e:
            log(LOG_ERROR, "Exception: " + str(e))
            pass

        try:
            log(LOG_INFO, "Create table message")
            Message.create_table()
            created_message = True
        except peewee.OperationalError as e:
            log(LOG_ERROR, "Exception: " + str(e))
            pass

        if created_user and created_message:
            print "Done"
            break
        else:
            retries = retries + 1
            if retries == max_retries:
                log(LOG_ERROR, "Exception: Couldn't connect to MySql yet.")
                return
            time.sleep(10)


if __name__ == '__main__':
    init_tables() 
