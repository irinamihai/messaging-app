import peewee
from peewee import *

from models import User, Message, init_tables

from threading import Thread, Lock

mutex = Lock()


def is_palindrome(n):
    return str(n) == str(n)[::-1]


def get_message(user_id=None, message_id=None):
    """
    Executes the GET command. Returns messages.

    :param user_id: The user ID
    :param message_id: The message ID
    :returns: List of all messages for that user or just one message, if
              message_id is provided
    :raises Exception: Rarely
    """
    # If neither the user, not the message ID have been provided, return.
    if not user_id:
        raise Exception("You must provide at least the user ID.")

    # Find user name
    username_query = User.select().where(User.id == user_id)

    if len(username_query) == 0:
        raise Exception("User ID not registered.")

    user_name = username_query[0].username

    if not message_id:
        # If only the user's ID is provided, return all the messages for that
        # specific user.
        user_messages = Message.select().where(Message.user_id == user_id)
    else:
        # If both the user and the message IDs are specified, return the
        # specific message for the user.
        user_messages = Message.select().where(
                Message.user_id == user_id, Message.id == message_id)
    # Return messages found.
    msgs = []
    for m in user_messages:
       msgs.append({
            "user_id": user_id,
            "user_name": user_name,
            "message_id": m.id,
            "message_text": m.text,
            "is_palindrome": is_palindrome(m.text)})
    return msgs

def get_users():
    """
    Executes the GET command. Returns users.

    :returns: List of all users
    :raises Exception: Rarely
    """

    query_users = User.select()
    
    # Return users found.
    users = []
    for u in query_users:
       users.append({
            "user_id": u.id,
            "user_name": u.username})
    return users

def post_message(user_id=None, text=None):
    """
    Executes the POST command.

    :param user_id: The user ID
    :param text: The message text
    :returns: True if succeeded
    :raises Exception: Sometimes
    """
    # If the user is not provided, return.
    if not user_id:
        raise Exception("You must provide at least the user ID.")

    # If the message is empty, return.
    if not text:
        raise Exception("Message cannot be empty.")

    # Find if user exists.
    query = User.select().where(User.id == user_id)
    if len(query) == 0:
        raise Exception("User doesn't exist.")
        
    # Create dictionary with message data.
    message = {"user_id": user_id, "text": text}

    # Add the new message to database.
    new_message = Message(**message)
    new_message.save()
    
    # Get new message ID.
    query = Message.select().where(Message.text == text,
                                   Message.user_id == user_id).order_by(Message.id.desc())
    if len(query) == 0:
        raise Exception("Message couldn't be added.")

    return query[0].id
    
def patch_message(user_id=None, message_id=None, new_message=None):
    """
    Executes the PATCH command.

    :param user_id: The user ID
    :param message_id: The message ID
    :param new_message: The new message text
    :returns: True if succeeded
    :raises Exception: Sometimes
    """
    # If the user or message id are missing, return.
    if not user_id or not message_id:
        raise Exception("For changing a message, you need both the " \
                        "user ID and the message ID.")

    # If the new message is empty, return.
    if not new_message:
        raise Exception("Your new message cannot be empty.")
        
    # Find initial message.
    message = Message.select().where(
        Message.user_id == user_id, Message.id == message_id)

    # If initial message wasn't found, return.
    if not message:
        raise Exception("Initial message does not exist.")
    
    # Save the edited message content.
    query = Message.update(text=new_message).where(
        Message.user_id == user_id, Message.id == message_id)
    query.execute()
    return True

def delete_message(user_id=None, message_id=None):
    """
    Executes the DELETE command. Removes a message.

    :param user_id: The user ID
    :param message_id: The message ID
    :returns: True if succeeded
    :raises Exception: Rarely
    """
    # If the user or message id are missing, return.
    if not user_id or not message_id:
        raise Exception("For deleting a message, you need both the " \
                        "user ID and the message ID.")

    # Find message.
    query = Message.select().where(
        Message.user_id == user_id, Message.id == message_id)

    # Attempt deletion.
    for entry in query:
        entry.delete_instance()
        return True # Should be just one entry

    # If control reaches here, then the message wasn't found.
    raise Exception("Message does not exist.")

def delete_all():
    """
    Executes the DELETE command. Cleans everything.

    :param user_id: The user ID
    :param message_id: The message ID
    :returns: True if succeeded
    :raises Exception: Never
    """

    mutex.acquire()
    try:
        # Delete messages
        Message.drop_table()
        # Delete users
        User.drop_table()
        # Recreate tables
        init_tables()
    finally:
        mutex.release()

    return True

def add_user(user_name=None):
    """
    Executes the PUT command which adds a new user.

    :param user_name: The username
    :returns: True if succeeded
    :raises Exception: Rarely
    """
    # If the user or message id are missing, return.
    if not user_name:
        raise Exception("Adding a new user requires a name.")

    # Find if username exists.
    query = User.select().where(User.username == user_name)

    if len(query) != 0:
        raise Exception("Username is taken.")

    # Add user.
    new_user = User(username=user_name)
    new_user.save()
    
    # Get user ID.
    query = User.select().where(User.username == user_name)
    if len(query) == 0:
        raise Exception("User couldn't be added.")

    return query[0].id
