import time
import datetime

LOG_NOTHING  = 0
LOG_ERROR    = 1
LOG_WARNING  = 2
LOG_INFO     = 3

prefix_strings = {LOG_ERROR:"ERROR", LOG_WARNING:"WARNING", LOG_INFO:"INFO"}
log_level = LOG_ERROR

log_file = None

def set_loglevel(level):
    global log_level
    if level <= LOG_NOTHING:
        log_level = LOG_NOTHING
    elif level >= LOG_INFO:
        log_level = LOG_INFO
    else:
        log_level = level
    return True

def set_logfile(logfile):
    global log_file
    log_file = logfile

def log(level, text):
    if level > log_level or level <= LOG_NOTHING:
        return False
    
    ts = time.time()
    msg = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') + " [" + prefix_strings[level] + "]: " + str(text)
    
    if log_file:
        with open(log_file, "a") as f:
            f.write(msg + "\n")
    else:
        print (msg)
        
    return True
