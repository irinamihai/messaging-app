from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from SocketServer import ThreadingMixIn
from models import init_tables
from model_execution import get_users
from model_execution import get_message
from model_execution import post_message
from model_execution import patch_message
from model_execution import delete_message
from model_execution import delete_all
from model_execution import add_user
import SocketServer
import SimpleHTTPServer
import threading
import argparse
import re
import cgi
import json

from msg_logging import *

class HTTPRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def send_back(self, code, response):
        """
        Sends response back to client

        :param code: The HTTP code
        :param response: The HTTP message
        """
        self.send_response(code)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(response))

    def send_usage(self):
        """
        Sends a 'usage' message back to client
        """
        usage = "Correct API use: "                      \
                "\"/api/<USERID>/messages\" or "         \
                "\"/api/<USERID>/messages/<MSGID>\" or " \
                "\"/api/users\" or "                     \
                "\"/api/add_user\" or "                  \
                "\"/api/dangerous/delete_everything\""
        self.send_back(403, usage)

    def do_POST(self):
        """
        POST handler
        """
        log(LOG_INFO, "Received POST, path: %s" % self.path)
        if (None != re.search('/api/(\d+)/messages', self.path) and 
            self.path.endswith('messages')):

            try:
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: content-type")
                return

            if ctype == 'application/json':
                length = int(self.headers.getheader('content-length', 0))
                data = self.rfile.read(length)
                json_msg = json.loads(data)
                log(LOG_INFO, "JSON msg: %s" % json_msg)
                if 'm' in json_msg:
                    user_id = re.search(r'\d+', self.path).group()
                    log(LOG_INFO, "POST user_id: %s, data: %s" % (user_id, json_msg['m']))
                    try:
                        result = post_message(user_id, json_msg['m'])
                    except Exception as e:
                        log(LOG_ERROR, "Exception: " + str(e))
                        self.send_back(500, "Internal server error: %s" % e)
                        return
                    if result:
                        self.send_back(200, "Message added successfully "
                                            "with ID: %s" % result)
                    else:
                        self.send_back(418, "Unknown error")
                else:
                    self.send_back(403, "Can't find the message key")
            else:
                self.send_back(403, "Content type is not application/json")
        else:
            self.send_usage()

    def do_GET(self):
        """
        GET handler
        """
        log(LOG_INFO, "Received GET, path: %s" % self.path)
        if (None != re.search('/api/users', self.path) and 
            self.path.endswith('users')):

            log(LOG_INFO, "Requesting all users")

            try:
                result = get_users()
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: %s" % e)
                return

            # All good
            self.send_back(200, result)
            log(LOG_INFO, "Sent all users")

        elif (None != re.search('/api/(\d+)/messages', self.path) and 
            self.path.endswith('messages')):

            user_id = re.search(r'\d+', self.path).group()
            log(LOG_INFO, "Requesting all messages for user %s" % user_id)

            try:
                result = get_message(user_id)
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: %s" % e)
                return

            # All good
            if not result:
                log(LOG_WARNING, "No messages found for user %s" % user_id)
                self.send_back(404, "No messages found for user %s" % user_id)
            else:
                self.send_back(200, result)
                log(LOG_INFO, "Sent all messages found for user %s" % user_id)

        elif (None != re.search('/api/(\d+)/messages/(\d+)', self.path) and 
            None != re.search(r'\d+$', self.path)):
            groups = re.findall('\d+', self.path)
            user_id = groups[0]
            message_id = groups[1]
            log(LOG_INFO, "Requesting message %s for user %s" % (message_id, user_id))

            try:
                result = get_message(user_id, message_id)
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: %s" % e)
                return

            # All good
            if not result:
                log(LOG_WARNING, "Message %s not found for user %s" %
                                 (message_id, user_id))
                self.send_back(404, "Message " + str(message_id) + " " +
                                    "not found for user " + str(user_id))
            else:
                self.send_back(200, result)
                log(LOG_INFO, "Sent message %s from user %s" % (message_id, user_id))
        else:
            self.send_usage()

    def do_PATCH(self):
        """
        PATCH handler
        """
        log(LOG_INFO, "Received PATCH, path: %s" % self.path)
        if (None != re.search('/api/(\d+)/messages/(\d+)', self.path) and 
            None != re.search(r'\d+$', self.path)):      

            groups = re.findall('\d+', self.path)
            user_id = groups[0]
            message_id = groups[1]

            try:
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: content-type")
                return

            if ctype == 'application/json':
                length = int(self.headers.getheader('content-length', 0))
                data = self.rfile.read(length)
                json_msg = json.loads(data)
                if 'm' in json_msg:
                    log(LOG_INFO, "PATCH user_id: %s, data: %s" % (user_id, json_msg['m']))
                    try:
                        result = patch_message(user_id, message_id, json_msg['m'])
                    except Exception as e:
                        log(LOG_ERROR, "Exception: " + str(e))
                        self.send_back(500, "Internal server error: %s" % e)
                        return
                    if result:
                        self.send_back(200, "Message changed successfully")
                    else:
                        self.send_back(418, "Unknown error")
                else:
                    self.send_back(403, "Can't find the message key")
            else:
                self.send_back(403, "Content type is not application/json")
        else:
            self.send_usage()

    def do_DELETE(self):
        """
        DELETE handler
        """
        log(LOG_INFO, "Received DELETE, path: %s" % self.path)
        if (None != re.search('/api/(\d+)/messages/(\d+)', self.path) and 
            None != re.search(r'\d+$', self.path)):
            # Deleting a message
            
            groups = re.findall('\d+', self.path)
            user_id = groups[0]
            message_id = groups[1]
            
            log(LOG_INFO, "Deleting message %s for user %s" % (message_id, user_id))
            
            try:
                result = delete_message(user_id, message_id)
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: %s" % e)
                return

            # All good
            if not result:
                log(LOG_WARNING, "Message %s not found for user %s" %
                                 (message_id, user_id))
                self.send_back(404, "Message " + str(message_id) + " " +
                                    "not found for user " + str(user_id))
            else:             
                self.send_back(200, "Message deleted")
                log(LOG_INFO, "Deleted message %s from user %s" % (message_id, user_id))

        elif (None != re.search('/api/dangerous/delete_everything', self.path) and 
            self.path.endswith('delete_everything')):
            # Deleting everything. Including users!  
    
            log(LOG_WARNING, "Deleting everything")
            
            try:
                result = delete_all()
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: %s" % e)
                return

            # All good
            if not result:
                log(LOG_ERROR, "Couldn't delete everything")
                self.send_back(500, "Couldn't delete everything")
            else:             
                self.send_back(200, "Everything deleted")
                log(LOG_INFO, "Everything deleted")
        else:
            self.send_usage()

    def do_PUT(self):
        """
        PUT handler
        """
        log(LOG_INFO, "Received PUT, path: %s" % self.path)
        if (None != re.search('/api/add_user', self.path) and 
            self.path.endswith('add_user')):
            
            log(LOG_INFO, "Adding new user")
            try:
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
            except Exception as e:
                log(LOG_ERROR, "Exception: " + str(e))
                self.send_back(500, "Internal server error: content-type")
                return

            if ctype == 'application/json':
                length = int(self.headers.getheader('content-length', 0))
                data = self.rfile.read(length)
                json_msg = json.loads(data)
                if 'user' in json_msg:
                    log(LOG_INFO, "PUT user: %s" % (json_msg['user']))
                    try:
                        result = add_user(json_msg['user'])
                    except Exception as e:
                        log(LOG_ERROR, "Exception: " + str(e))
                        self.send_back(500, "Internal server error: %s" % e)
                        return
                    if result:
                        self.send_back(200, "User '%s' added successfully "  
                                            "with ID: %s" % (json_msg['user'], result))
                    else:
                        self.send_back(418, "Unknown error")
                else:
                    self.send_back(403, "Can't find the user key")
            else:
                self.send_back(403, "Content type is not application/json")
        else:
            self.send_usage()

if __name__=='__main__':

    set_loglevel(LOG_INFO)
    set_logfile("server_log.txt")
    
    init_tables()
    
    parser = argparse.ArgumentParser(description='HTTP Server')
    parser.add_argument('port', type=int, help='Listening port for HTTP Server')
    parser.add_argument('ip', help='HTTP Server IP')
    args = parser.parse_args()

    httpd = SocketServer.ThreadingTCPServer((args.ip, args.port), HTTPRequestHandler)

    log(LOG_INFO, 'HTTP Server Started')

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    log(LOG_INFO, 'HTTP Server Closed')

