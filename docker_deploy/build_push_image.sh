#!/bin/bash

function _usage {
    echo "Usage: ./build_push_image.sh <version> <docker_username> <docker_repo>"
}

echo $#

if [ $# -ne 3 ];
then
    _usage
    exit 1
fi

VERSION=$1
USERNAME=$2
REPO=$3
docker build -t msg_api_v${VERSION} .
docker tag msg_api_v${VERSION} $USERNAME/$REPO:msg_api_v${VERSION}
docker image ls
docker push $USERNAME/$REPO:msg_api_v${VERSION}

exit 0
