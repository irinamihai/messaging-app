#!/bin/bash

chmod +x ./case*

function test_failed {
    echo "================"
    echo " Testing failed "
    echo "================"
    exit 1
}

function test_success {
    echo "===================="
    echo " Testing successful "
    echo "===================="
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi


echo "============ Case000 SANITY TESTS ============="

./case001_server_connection.sh $1
if [ "$?" != 0 ]; then test_failed; fi

echo "======= Case000 SANITY TESTS Successful ======="


# Starting with a new database
../client $1 DELETE ALL

echo "============== Case100 APP TESTS =============="

./case101_users_and_messages.sh $1
if [ "$?" != 0 ]; then test_failed; fi

./case102_editing_messages.sh $1
if [ "$?" != 0 ]; then test_failed; fi

./case103_deleting_messages.sh $1
if [ "$?" != 0 ]; then test_failed; fi

echo "======== Case100 APP TESTS Successful ========="



echo "============== Case200 API TESTS =============="

./case201_in_depth.sh $1
if [ "$?" != 0 ]; then test_failed; fi

echo "======== Case200 API TESTS Successful ========="


# Cleaning database
../client $1 DELETE ALL


test_success
