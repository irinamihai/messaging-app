#!/bin/bash

function test_failed {
    echo "#### Case201 failed."
    exit 1
}

function test_success {
    echo "#### Case201 successful."
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi

echo "###############################################"
echo "## Running Case201: Testing API in-depth       "
echo "###############################################"

echo ">>>> TEST: Calling POST unknown API. With JSON content."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/unknown -H Content-Type:application/json -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling POST unknown API. Without JSON content."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/unknown)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling POST good API. With invalid user ID."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/_invalid_/messages)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling POST good API. With valid user ID. No JSON content."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/1/messages)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling POST good API. With valid user ID. Invalid JSON content."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/1/messages -H Content-Type:application/json -d "{\"_invalid_\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling POST good API. With valid user ID. Invalid Content-Type."
# Should fail.
res=$(curl -X POST -sw "%{http_code}" http://$1/api/1/messages -H Content-Type:application/octet-stream -d "{\"_invalid_\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi



echo ">>>> TEST: Calling GET unknown API. With JSON content."
# Should fail.
res=$(curl -X GET -sw "%{http_code}" http://$1/api/unknown -H Content-Type:application/json -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling GET unknown API. Without JSON content."
# Should fail.
res=$(curl -X GET -sw "%{http_code}" http://$1/api/unknown)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling GET good API. With invalid userID."
# Should fail.
res=$(curl -X GET -sw "%{http_code}" http://$1/api/_invalid_/messages)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling GET good API. With valid userID. Invalid message ID."
# Should fail.
res=$(curl -X GET -sw "%{http_code}" http://$1/api/1/messages/_invalid)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling GET good API. With valid userID. Missing message ID."
# Should fail.
res=$(curl -X GET -sw "%{http_code}" http://$1/api/1/messages/)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi



echo ">>>> TEST: Calling PATCH unknown API. With JSON content."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/unknown -H Content-Type:application/json -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PATCH unknown API. Without JSON content."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/unknown)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PATCH good API. Without invalid userID. Message 3 exists from other tests."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/_invalid_/messages/3)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PATCH good API. With valid userID. Invalid Message ID."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/1/messages/_invalid_)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PATCH good API. With valid userID. Valid Message ID. Invalid JSON content."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/1/messages/3 -H Content-Type:application/json -d "{\"_invalid_\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PATCH good API. With valid userID. Valid Message ID. Invalid Content-Type."
# Should fail.
res=$(curl -X PATCH -sw "%{http_code}" http://$1/api/1/messages/3 -H Content-Type:application/octet-stream -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi



echo ">>>> TEST: Calling PUT unknown API. With JSON content."
# Should fail.
res=$(curl -X PUT -sw "%{http_code}" http://$1/api/unknown -H Content-Type:application/json -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PUT unknown API. Without JSON content."
# Should fail.
res=$(curl -X PUT -sw "%{http_code}" http://$1/api/unknown)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PUT good API. Invalid JSON content."
# Should fail.
res=$(curl -X PUT -sw "%{http_code}" http://$1/api/add_user -H Content-Type:application/json -d "{\"_invalid_\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling PUT good API. Invalid Content-Type."
# Should fail.
res=$(curl -X PUT -sw "%{http_code}" http://$1/api/add_user -H Content-Type:application/octet-stream -d "{\"user\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi



echo ">>>> TEST: Calling DELETE unknown API. With JSON content."
# Should fail.
res=$(curl -X DELETE -sw "%{http_code}" http://$1/api/unknown -H Content-Type:application/json -d "{\"m\":\"Test1\"}")
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling DELETE unknown API. Without JSON content."
# Should fail.
res=$(curl -X DELETE -sw "%{http_code}" http://$1/api/unknown)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling DELETE good API. Invalid user. Message 3 exists from other tests."
# Should fail.
res=$(curl -X DELETE -sw "%{http_code}" http://$1/api/_invalid_/messages/3)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

echo ">>>> TEST: Calling DELETE good API. Valid user. Invalid message ID."
# Should fail.
res=$(curl -X DELETE -sw "%{http_code}" http://$1/api/1/messages/_invalid_)
http_code="${res:${#res}-3}"
if [ "$http_code" == 200 ]; then test_failed; fi

test_success
