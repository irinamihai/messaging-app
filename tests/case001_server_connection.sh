#!/bin/bash

function test_failed {
    echo "#### Case001 failed."
    exit 1
}

function test_success {
    echo "#### Case001 successful."
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi

echo "###############################################"
echo "## Running Case001: Testing server connection  "
echo "###############################################"

IP=$(echo $1 | cut -d: -f1)
PORT=$(echo $1 | cut -d: -f2)

echo ">>>> TEST: Sending a command to an unexisting app port should return code 0. 47 is an unused port."
# Should fail.
./../client $IP:47 GET 1 1
if [ "$?" != 0 ]; then test_failed; fi

echo ">>>> TEST: Sending a command to an existing app port should return a positive code. App is alive."
# Should succeed.
./../client $IP:$PORT GET 1 1
if [ "$?" == 0 ]; then test_failed; fi

test_success
