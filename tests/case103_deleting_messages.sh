#!/bin/bash

function test_failed {
    echo "#### Case103 failed."
    exit 1
}

function test_success {
    echo "#### Case103 successful."
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi

echo "###############################################"
echo "## Running Case103: Testing message deletion   "
echo "###############################################"

echo ">>>> TEST: Deleting a message from a user that doesn't exist."
# Should fail.
./../client $1 DELETE 3 1
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Deleting a message that doesn't exist from an existing user."
# Should fail.
./../client $1 DELETE 1 4
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Deleting an existing message from an existing user."
# Should succeed.
./../client $1 DELETE 1 1
if [ "$?" != 200 ]; then test_failed; fi

test_success
