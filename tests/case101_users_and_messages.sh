#!/bin/bash

function test_failed {
    echo "#### Case101 failed."
    exit 1
}

function test_success {
    echo "#### Case101 successful."
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi

echo "###############################################"
echo "## Running Case101: Testing users and messages "
echo "###############################################"

echo ">>>> TEST: Getting a message for a user that doesn't exist."
# Should fail.
./../client $1 GET 1 1
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Adding a new user."
# Should succeed.
./../client $1 PUT "TestUser1"
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Getting a message that doesn't exist, for a user that exists."
# Should fail.
./../client $1 GET 1 1
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Posting a new message for a user that doesn't exist."
# Should fail.
./../client $1 POST 3 "Message1"
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Posting a new message for a user that exists."
# Should succeed.
./../client $1 POST 1 "Message1"
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Getting a message that exists for a user that exists."
# Should succeed.
./../client $1 GET 1 1
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Getting a message that doesn't exist, for a user that exists, after another message has been inserted."
# Should fail.
./../client $1 GET 1 2
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Adding a new user."
# Should succeed.
./../client $1 PUT "TestUser2"
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Adding a user that already exists."
# Should fail.
./../client $1 PUT "TestUser2"
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Posting message as the new user just added."
# Should succeed.
./../client $1 POST 2 "Message2"
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Getting message #2 for user #1."
# Should fail, because it's different from getting message #1 for user #2 that would succeed.
./../client $1 GET 1 2
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Posting 2nd message for user #1."
# Should succeed.
./../client $1 POST 1 "Message3"
if [ "$?" != 200 ]; then test_failed; fi

echo ">>>> TEST: Getting 2nd message for user #1 should now succeed, whereas the previous similar call should have failed. Second message is now message #3."
# Should succeed.
./../client $1 GET 1 3
if [ "$?" != 200 ]; then test_failed; fi

test_success
