#!/bin/bash

function test_failed {
    echo "#### Case102 failed."
    exit 1
}

function test_success {
    echo "#### Case102 successful."
    exit 0
}

if [ -z "$1" ]; then
    echo Need server parameter.
    test_failed
fi

echo "###############################################"
echo "## Running Case102: Testing message editing    "
echo "###############################################"

echo ">>>> TEST: Patching a message from a user that doesn't exist."
# Should fail.
./../client $1 PATCH 3 1 "Message1_Patched"
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Patching a message that doesn't exist from an existing user."
# Should fail.
./../client $1 PATCH 1 5 "Message5_Patched"
if [ "$?" == 200 ]; then test_failed; fi

echo ">>>> TEST: Patching an existing message from an existing user."
# Should succeed.
./../client $1 PATCH 2 2 "Message2_Patched"
if [ "$?" != 200 ]; then test_failed; fi

test_success
