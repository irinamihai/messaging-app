# Small Messaging Application

## Getting Started

These instructions will explain how to get a copy of the project up and running on a local machine.
See deployment for notes on how to deploy the project on a live system.

The application package can be obtained from BitBucket:
```
$ git clone https://irinamihai@bitbucket.org/irinamihai/messaging-app.git
$ cd messaging-app/
```

## Architecture and content
The messaging application exposes a REST API that allows users to manage messages.
The information is kept in a MySQL database, ```message``` that has two tables, ```user``` and ```message```.

### Supported operations
The following operations can be made using a client made available in the package:

- Add a new user

- List all users

- Sending a new message

- Retrieving a message for one user

- Retrieving all messages from one user

- Update message for one user

- Delete message for one user

- Delete everything (users and messages): Used for testing purposes only.

### Used packages
The following technologies have been used:

- MySQL for storing the application's data

- Peewee ORM for managing the user and message data

- Docker for building an image containing the application

- Kubernetes for deploying the application

- bash for install automation and testing scripts

- python for writing the server application

### Application package
The package contains the following components:

- docker_deploy: directory that contains everything needed to create a docker image with the messaging application
	
	-- Dockerfile

	-- src: directory containing the source code of the messaging application

	-- build_push_image.sh: script used to build, tag and push the image to a docker repository. Usage: `$ sudo ./build_push_image.sh <version> <docker_username> <docker_repo>`

- k8s: directory with all the needed .yaml files to deploy the application on k8s. The api.yaml file containg the Docker image with the messaging application

	-- image: irinamihai/niceandeasy:msg_api_v22
       

- client: wrapper to easily access the API

- tests: directory containing the testing framework

	-- run_all.sh: script to run all the tests

	-- case*_*.sh: testcases


## Usage options

It is assumed that the application will be deployed and run on a Linux environment.

The application can be used in two ways:

- deploying it on a Kubernetes cluster

- directly calling the API, available via the following DNS ec2-18-188-211-47.us-east-2.compute.amazonaws.com:80, using the provided client. The application is already deployed on AWS EC2.
	
```
$ ./client ec2-18-188-211-47.us-east-2.compute.amazonaws.com:80 PUT Alice
HTTP: 200
"User 'Alice' added successfully with ID: 1"
```

## Deploying with Kubernetes

### Environment setup

If the application will be deployed with Kubernetes, it is assumed that you have one cluster configured and running.
If not, you can find below some instructions on how to install and start a leightweight version of Kubernetes, minikube and the Kubernetes command line interface, kubectl.

If you need to install minikube and kubectl, follow the instructions below:

```
$ sudo apt-get update
$ sudo apt-get install curl
$ sudo apt-get install virtualbox virtualbox-ext-pack
$ curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
$ chmod +x minikube
$ sudo mv -v minikube /usr/local/bin

$ curl -Lo kubectl https://storage.googleapis.com/kubernetes-release/release/v1.8.0/bin/linux/amd64/kubectl
$ chmod +x kubectl
$ sudo mv -v kubectl /usr/local/bin

$ minikube start
```

Once you have a Kubernetes environment setup, all you need to do is run the ```deploy_k8s``` script.
This script will deploy MySQL and the messaging application.
All the Kubernetes resources are created in the msg-api namespace.

```
$ ./deploy_k8s configure
Deploy messaging API service
namespace/msg-api created
==========================================
Create k8s secrets
==========================================
configmap/db created
secret/mysql-pass created
==========================================
Create k8s persistent volumes
==========================================
persistentvolume/mysql-volume created
persistentvolume/msg-api-volume created
==========================================
Deploy the application
==========================================

> Deploy MySQL

service/mysql created
persistentvolumeclaim/mysql-pvc created
deployment.apps/mysql created
Wait for pod mysql to complete
No resources found.

> Deploy the API

service/msg-api created
persistentvolumeclaim/msg-api-pvc created
deployment.apps/msg-api created
Wait for pod msg-api to complete
DONE

```

Once this is done, you can check the the deployments and pods and present and running:
```
$ kubectl get deployment -n msg-api
NAME      DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
msg-api   1         1         1            1           11m
mysql     1         1         1            1           11m

$ kubectl get pod -n msg-api
NAME                       READY   STATUS    RESTARTS   AGE
msg-api-857fbfbff8-9n2d4   1/1     Running   0          11m
mysql-7bf9966ff5-tnf77     1/1     Running   0          12m
```
### Accessing the API

The API is exposed through the msg-api service

```
$ kubectl get service -n msg-api msg-api
NAME      TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
msg-api   LoadBalancer   10.103.166.42   <pending>     8080:32285/TCP   2m
```

If you are using minikube, use the following command to determine the IP and port to use when calling the API client.
```
$ minikube service -n msg-api msg-api --url
http://192.168.99.100:32285
```

For a custom Kubernetes cluster that doesn't provide a LoadBalancer, use the node's IP and the port translated by the service. In the example above, 32285.
Another option would be to expose the service through a NodePort, as below.
```
$ kubectl expose deployment msg-api -n msg-api --type=NodePort --name=msg-api-node-port
service/msg-api-node-port exposed
$ kubectl get service -n msg-api
NAME                TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
msg-api             LoadBalancer   10.107.11.36   <pending>     8080:31074/TCP   15m
msg-api-node-port   NodePort       10.104.42.74   <none>        8080:32490/TCP   9s
mysql               ClusterIP      None           <none>        3306/TCP         15m
```

### Remove application
For removing all the Kubernetes resources for the messaging application, you need to run the same ```deploy_k8s``` script, but with a different option.
```
$ ./deploy_k8s clean
Remove messaging API service
==========================================
Delete the messaging API service
==========================================

> Delete the deployments

deployment.extensions "msg-api" deleted
deployment.extensions "mysql" deleted

> Delete persistent volume claims

persistentvolumeclaim "msg-api-pvc" deleted
persistentvolumeclaim "mysql-pvc" deleted

> Delete persistent volumes

persistentvolume "msg-api-volume" deleted
persistentvolume "mysql-volume" deleted

> Delete secrets

secret "mysql-pass" deleted

> Delete the config map

configmap "db" deleted

> Delete services

service "mysql" deleted
service "msg-api" deleted
Wait for pod msg-api to terminate
No resources found.
Wait for pod mysql to terminate
No resources found.

> Delete namespace

namespace "msg-api" deleted
DONE

```

### End to end tests

Testing is done in 3 stages. If an error occurs at any time, test execution is stopped.

- First stage tests for server connection and it also tests the client being able to return correct error codes which are necessary for the next stages.

```
============ Case000 SANITY TESTS =============
###############################################
## Running Case001: Testing server connection
###############################################
>>>> TEST: Sending a command to an unexisting app port should return code 0. 47 is an unused port.
HTTP: 000
No JSON object could be decoded
>>>> TEST: Sending a command to an existing app port should return a positive code. App is alive.
HTTP: 500
"Internal server error: User ID not registered."
#### Case001 successful.
======= Case000 SANITY TESTS Successful =======
```

- Second stage tests for the ability to add new users, send and receive messages, editing existing messages and deleting them. Here, the app functionality is tested.

```
============== Case100 APP TESTS ==============                                                                     
###############################################                                                                     
## Running Case101: Testing users and messages                                                                      
###############################################                                                                     
>>>> TEST: Getting a message for a user that doesn't exist.                                                         
HTTP: 500                                                                                                           
"Internal server error: User ID not registered."                                                                    
>>>> TEST: Adding a new user.                                                                                       
HTTP: 200                                                                                                           
"User 'TestUser1' added successfully with ID: 1"
[...]
>>>> TEST: Deleting an existing message from an existing user.
HTTP: 200
"Message deleted"
#### Case103 successful.
======== Case100 APP TESTS Successful =========
```

- Third and last stage is directly calling 'curl' and creating different malformed API requests in order to test the error checking. A few bugs were caught due to this stage.
```
============== Case200 API TESTS ==============                                                                     
###############################################                                                                     
## Running Case201: Testing API in-depth
###############################################
>>>> TEST: Calling POST unknown API. With JSON content.
>>>> TEST: Calling POST unknown API. Without JSON content.
>>>> TEST: Calling POST good API. With invalid user ID.
>>>> TEST: Calling POST good API. With valid user ID. No JSON content.
[...]
>>>> TEST: Calling PUT good API. Invalid Content-Type.
>>>> TEST: Calling DELETE unknown API. With JSON content.
>>>> TEST: Calling DELETE unknown API. Without JSON content.
>>>> TEST: Calling DELETE good API. Invalid user. Message 3 exists from other tests.
>>>> TEST: Calling DELETE good API. Valid user. Invalid message ID.
#### Case201 successful.
======== Case200 API TESTS Successful =========
```

#### Running the tests

Execute run_all.sh as following:

- Example for the Amazon EC2 instance:
```
$ cd tests/
$ ./run_all.sh ec2-18-188-211-47.us-east-2.compute.amazonaws.com:80
```

- Example for a minikube deployment:
```
$ minikube service -n msg-api msg-api --url
http://192.168.99.100:31952
$ cd tests/
$ ./run_all.sh 192.168.99.100:31952
```

## Notes
* For the Kubernetes deployment and if running on a minikube setup or on a slow cluster, it will take longer for the pods to spawn and for the services to completely configure. Because of this limitation, it is advisable to give the pods around 5 or 10 minutes.
To make sure the messaging service has started on the pod, the following can be done:
```
$ kubectl get pod -n msg-api
NAME                       READY   STATUS    RESTARTS   AGE
msg-api-857fbfbff8-zfssw   1/1     Running   0          2m
mysql-7bf9966ff5-gbvtx     1/1     Running   0          3m

# Note the name of the msg-api pod: msg-api-857fbfbff8-zfssw

# Check the application logs and make sure the needed tables have been created and the server has been started.
$ kubectl exec -it msg-api-857fbfbff8-zfssw /bin/bash -n msg-api
root@msg-api-857fbfbff8-zfssw:/msg_api# ls
docker_requirements.txt  model_execution.py  model_execution.pyc  models.py  models.pyc  msg_logging.py  msg_logging.pyc  server.py  server_log.txt
root@msg-api-857fbfbff8-zfssw:/msg_api# tail -f server_log.txt 
2018-10-22 01:55:54 [ERROR]: Exception: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
2018-10-22 01:55:54 [INFO]: Create table message
2018-10-22 01:55:54 [ERROR]: Exception: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
2018-10-22 01:56:04 [INFO]: Create table users
2018-10-22 01:56:04 [ERROR]: Exception: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
2018-10-22 01:56:04 [INFO]: Create table message
2018-10-22 01:56:04 [ERROR]: Exception: (2003, "Can't connect to MySQL server on 'mysql' ([Errno 111] Connection refused)")
2018-10-22 01:56:14 [INFO]: Create table users
2018-10-22 01:56:17 [INFO]: Create table message
2018-10-22 01:56:19 [INFO]: HTTP Server Started
```

* Examples on accessing the API both via the client or directly using 'curl' can be found inside the testcases directory, ```tests```.

## Annex: REST API functionality and usage details

The REST API functionality is based on HTTP methods.

Operation | REST API | Using the client | Details |
--- | --- | --- | --- |
Creating a new user | PUT /api/add_user | client `<server:port>` PUT `<new_user>` | Content-Type is application/json, formatted {"user": `<new_user>`} |
Sending a new message | POST /api/`<user_id>`/messages | client `<server:port>` POST `<user_id>` `<message_text>` | Content-Type is application/json, formatted {"m": `<new_message_text>`} |
Editing existing message | PATCH /api/`<user_id>`/messages/`<existing_message_id>` | client `<server:port>` PATCH `<user_id>` `<existing_message_id>` `<new_message_text>` | Content-Type is application/json, formatted {"m": `<new_message_text>`} |
Deleting a message | DELETE /api/`<user_id>`/messages/`<existing_message_id>` | client `<server:port>` DELETE `<user_id>` `<existing_message_id>` | - |
Requesting a list with all users | GET /api/users | client `<server:port>` GET USERS | - |
Requesting all messages for an user | GET /api/`<user_id>`/messages | client `<server:port>` GET `<user_id>` | - |
Requesting a single messages for an user | GET /api/`<user_id>`/messages/`<existing_message_id>` | client `<server:port>` GET `<user_id>` `<existing_message_id>` | - |
Deleting everything | DELETE /api/dangerous/delete_everything | client `<server:port>` DELETE ALL | Will delete and recreate the MySQL tables |

## Authors

* **Irina Mihai**

## Acknowledgments

* I'd like to thank the food delivery industry simply for its existence
